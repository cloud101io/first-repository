variable "instance_type" {
    default = "t3.micro"
}

variable "security_group" {
    default = ""
}