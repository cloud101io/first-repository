resource "aws_security_group" "web" {
    name = "web-sg"

    ingress = [
        {
            description = "ssh"
            from_port   = 22
            to_port     = 22
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
            ipv6_cidr_blocks    = ["::/0"]
            prefix_list_ids = null
            self = null
            security_groups = null
        }
    ]

    egress = [
        {
            description = "allow_all_egress"
            from_port   = 0
            to_port     = 0
            protocol    = "-1"
            cidr_blocks = ["0.0.0.0/0"]
            ipv6_cidr_blocks    = ["::/0"]
            prefix_list_ids = null
            self = null
            security_groups = null
        }
    ]
}