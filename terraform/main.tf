provider "aws" {
    region  = var.region
}

module "ec2" {
    source  = "./modules/ec2"
    security_group = module.security_groups.web
}

module "security_groups" {
    source  = "./modules/security-groups"
}