#!/bin/bash
apt install -y apache2

#Add logic to pull down index.html from corresponding region bucket
if [ $(curl http://169.254.169.254/latest/meta-data/placement/region) == "us-east-1" ]; then
    cd /var/www/html/ && rm index.html && wget https://cloud101-bucket.s3.amazonaws.com/index.html && service apache2 restart
elif [ $(curl http://169.254.169.254/latest/meta-data/placement/region) == "us-east-2" ]; then
    cd /var/www/html/ && rm index.html && wget https://cloud101-bucket2.s3.us-east-2.amazonaws.com/index.html && service apache2 restart
else
    echo "Region not found."
fi

#Add ownership permissions to apache files
for file in /var/www/html/*; do
    chown www-data:www-data $file
    chmod 775 $file
    echo "Permissions set on $file"
done