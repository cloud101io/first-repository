#!/bin/bash
apt update
apt install ansible -y
apt install git -y
mkdir /opt/ansible/
cd /opt/ansible/ && git clone https://cloud101io@bitbucket.org/cloud101io/first-repository.git
ansible-playbook --connection=local /opt/ansible/first-repository/main.yml